package com.example.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.hibernate.LazyInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.JpaDemoApplication;
import com.example.model.Intern;
import com.example.model.Manager;

import io.sniffy.Sniffer;
import io.sniffy.Spy;
import io.sniffy.Threads;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JpaDemoApplication.class)
public class ManageManagerTests {

	@Autowired
	ManageManager manageManager;
	
	
	@Test
	public void findManagerByNamedQuery(){
		Spy spy = Sniffer.spy();
		Manager manager = manageManager.findByNamedQuery(12);
		assertNotNull(manager);
		// relationship with Trainee is eager, hibernate load the list because the named query don't.
        assertEquals(2, spy.executedStatements());
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
	@Test
	public  void findManagerById(){
		Spy spy = Sniffer.spy();
		Manager manager = manageManager.findById(12);
		assertNotNull(manager);
		// relationship with Trainee is eager, hibernate load the list because the named query don't.
        assertEquals(1, spy.executedStatements());
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
	@Test
	public  void findManagerByUnit(){
		Spy spy = Sniffer.spy();
		List<Manager> managers = manageManager.findByUnit("unit1");
		assertNotNull(managers);
		assertFalse(managers.isEmpty());
		// relationship with Trainee is eager, hibernate load the list because SpringData don't.
        assertEquals(3, spy.executedStatements());
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
}
