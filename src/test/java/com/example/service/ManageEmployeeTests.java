package com.example.service;

import static org.junit.Assert.*;

import org.hibernate.LazyInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.example.JpaDemoApplication;
import com.example.model.Intern;
import com.example.model.SalaryHistory;

import io.sniffy.Sniffer;
import io.sniffy.Spy;
import io.sniffy.Threads;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = JpaDemoApplication.class)
public class ManageEmployeeTests {

	@Autowired
	ManageEmployee manageEmployee;
	
	
	@Test
	public void findAsok(){
		Spy spy = Sniffer.spy();
		Intern asok = manageEmployee.retrieveInternByNamedQuery().get(0);
		assertNotNull(asok);
		assertEquals(asok.firstName, "Asok");
		try{
			asok.salaryHistory.size();
			assertFalse(true);
		} catch (LazyInitializationException e){
			// the list salaryHistory wasn't loaded by hibernate (lazy)
		}
		// relationship with SalaryHistory is lazy, hibernate doesn't load the list.
        assertEquals(1, spy.executedStatements());
        // Sniffer.verifyAtMostOnce() throws an AssertionError if more than one query was executed;
        spy.verifyAtMostOnce();
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
	@Test
	public void findInternPerId(){
		Spy spy = Sniffer.spy();
		Intern asok = manageEmployee.findInternById(10);
		assertNotNull(asok);
		assertEquals(asok.firstName, "Asok");
		try{
			asok.salaryHistory.size();
			assertFalse(true);
		} catch (LazyInitializationException e){
			// the list salaryHistory wasn't loaded by hibernate (lazy)
		}
		// relationship with SalaryHistory is lazy, hibernate doesn't load the list.
        assertEquals(1, spy.executedStatements());
        // Sniffer.verifyAtMostOnce() throws an AssertionError if more than one query was executed;
        spy.verifyAtMostOnce();
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
	@Test
	public void findSalaryPerId(){
		Spy spy = Sniffer.spy();
		SalaryHistory salary = manageEmployee.findSalaryById(20);
		assertNotNull(salary);
		assertNotNull(salary.employee);
		
        assertEquals(1, spy.executedStatements());
        // Sniffer.verifyAtMostOnce() throws an AssertionError if more than one query was executed;
        spy.verifyAtMostOnce();
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
	@Test
	public void findSalaryPerNamedQuery(){
		Spy spy = Sniffer.spy();
		SalaryHistory salary = manageEmployee.findSalaryByNamedQuery(20);
		assertNotNull(salary);
		assertNotNull(salary.employee);
		// relationship with SalaryHistory is lazy, hibernate doesn't load the list.
        assertEquals(2, spy.executedStatements());
        // Sniffer.verifyAtMostOnce() throws an AssertionError if more than one query was executed;
        spy.verifyAtMost(2);
        // Sniffer.verifyNever(Threads.OTHERS) throws an AssertionError if at least one query was executed
        // by the thread other than then current one
        spy.verifyNever(Threads.OTHERS);
	}
	
}
