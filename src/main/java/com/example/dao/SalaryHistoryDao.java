package com.example.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.model.SalaryHistory;



public interface SalaryHistoryDao extends CrudRepository<SalaryHistory, Integer> {

	
	@Query("select sh from SalaryHistory as sh where sh.id=?1")
	SalaryHistory findById(Integer id);
}
