package com.example.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.CrudMethodMetadata;
import org.springframework.data.repository.CrudRepository;

import com.example.model.Manager;
import com.example.model.SalaryHistory;

public interface ManagerDao extends CrudRepository<Manager, Integer> {

	@Query("select mg from Manager as mg where mg.id=?1")
	Manager findByIdViaNamedQuery(Integer id);
		
	
	List<Manager> findByUnit(String unit);
	
	//@Query("select mg from Manager as mg LEFT JOIN Trainee as tr on (mg.id = tr.manager) where mg.unit =?1 ")
	//List<Manager> findByUnitViaNamedQuery(String unit);
}
