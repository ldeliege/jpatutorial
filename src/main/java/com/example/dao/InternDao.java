package com.example.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.example.model.Intern;

public interface InternDao extends CrudRepository<Intern, Integer> {

	@Query("select i from Intern as i where  i.firstName=?1")
	List<Intern> findInternPerName(String firstname);
	
}
