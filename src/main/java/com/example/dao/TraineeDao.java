package com.example.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.model.Trainee;

public interface TraineeDao extends CrudRepository<Trainee, Integer> {

}
