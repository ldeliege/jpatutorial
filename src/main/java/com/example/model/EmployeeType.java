package com.example.model;

public enum EmployeeType {

	TRAINEE(Values.TRAINEE),
	INTERN(Values.INTERN),
	MANAGER(Values.MANAGER),
	DIRECTOR(Values.DIRECTOR);
	
	private String value;
	
	EmployeeType(String value){
		this.value=value;
	}
	
	
	public String toString(){
		return value;
	}
	
    public static class Values {
        public static final String TRAINEE = "T";
        public static final String INTERN = "I";
        public static final String MANAGER = "M";
        public static final String DIRECTOR = "D";
    }  

}
