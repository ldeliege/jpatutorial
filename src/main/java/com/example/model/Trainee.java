package com.example.model;


import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
@DiscriminatorValue(value=EmployeeType.Values.TRAINEE)
public class Trainee extends Employee {

	
	//note why "nullable = true"
	//we have only one table employee mapping everything.  except the trainee, all other type of employee
	//don't have a "manager".  Then, the constraint must be nullable
	//The check which verify that the manager is properly set on each trainee must be done
	//by the service.
	@ManyToOne()
	@JoinColumn(name="managerId", nullable=true, updatable=false)
	public Manager manager;
	
}
