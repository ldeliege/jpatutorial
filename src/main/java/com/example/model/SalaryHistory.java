package com.example.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class SalaryHistory {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	public Integer id;
	
	@Column(name="periodStartDate", nullable=false)
	public Date periodStartDate;
	
	@Column(name="periodEndDate")
	public Date periodEndDate;
	
	@Column(name="salary", nullable=false)
	public Integer salary;
		
	@ManyToOne
	@JoinColumn(name="employeeId", nullable=false, updatable=false)
	public Employee employee;
	
}
