package com.example.model;

import java.sql.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.hibernate.FetchMode;
import org.hibernate.annotations.FetchProfile.FetchOverride;

@Entity
@DiscriminatorValue(value=EmployeeType.Values.MANAGER)
public class Manager extends Employee {

	@Column(name="unit", nullable=true)
	public String unit;
	
	@OneToMany(fetch=FetchType.EAGER, mappedBy="manager")
	List<Trainee> trainees;
	
}
