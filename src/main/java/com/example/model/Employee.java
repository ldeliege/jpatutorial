package com.example.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;



@Entity
@DiscriminatorColumn(name="type")
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
public abstract class Employee {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	public Integer id;
	
	@Column(name="firstName", nullable=false)
	public String firstName;
	
	@Column(name="surname", nullable=false)
	public String surname;
	
	@Column(name="hiringDate", nullable=false)
	public Date hiringDate;
	
	@Column(name="endContractDate")
	public Date endContractDate;

}
