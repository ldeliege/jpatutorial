package com.example.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;


@Entity
@DiscriminatorValue(value=EmployeeType.Values.INTERN)
public class Intern extends Employee {

	@Column(name="salary")
	public Integer salary;
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="employee")
	public List<SalaryHistory> salaryHistory;
		
	
}
