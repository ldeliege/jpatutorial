package com.example.service;

import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.dao.ManagerDao;
import com.example.dao.SalaryHistoryDao;
import com.example.dao.TraineeDao;
import com.example.model.Intern;
import com.example.model.Manager;
import com.example.model.SalaryHistory;
import com.example.model.Trainee;

@Component
public class ManageManager {

	@Autowired
	private ManagerDao managerDao;
	
	@Autowired
	private TraineeDao traineeDao;
	
	
	@PostConstruct
	public void init(){
		Manager manager = new Manager();
		manager.unit = "unit1";
		manager.firstName = "The";
		manager.surname = "Manager";
		manager.hiringDate = Date.valueOf("2006-12-15");
		managerDao.save(manager);
	
		Trainee trainee = new Trainee();
		trainee.firstName = "NofirstName";
		trainee.surname = "Nooneknows";
		trainee.hiringDate = Date.valueOf("2016-7-10");
		trainee.manager = manager;
		traineeDao.save(trainee);
		
		trainee = new Trainee();
		trainee.firstName = "NofirstName-1";
		trainee.surname = "Nooneknows-1";
		trainee.hiringDate = Date.valueOf("2016-7-10");
		trainee.manager = manager;
		traineeDao.save(trainee);
			
		manager = new Manager();
		manager.unit = "unit1";
		manager.firstName = "The Other";
		manager.surname = "Manager";
		manager.hiringDate = Date.valueOf("2006-12-15");
		managerDao.save(manager);
	
		trainee = new Trainee();
		trainee.firstName = "NofirstName-2";
		trainee.surname = "Nooneknows-2";
		trainee.hiringDate = Date.valueOf("2016-7-10");
		trainee.manager = manager;
		traineeDao.save(trainee);
		
		trainee = new Trainee();
		trainee.firstName = "NofirstName-3";
		trainee.surname = "Nooneknows-3";
		trainee.hiringDate = Date.valueOf("2016-7-10");
		trainee.manager = manager;
		traineeDao.save(trainee);

	}
	
	public Manager findByNamedQuery(Integer id){
		return managerDao.findByIdViaNamedQuery(id);
	}
	
	public Manager findById(Integer id){
		return managerDao.findOne(id);
	}
	
	public List<Manager> findByUnit(String unit){
		return managerDao.findByUnit(unit);
	}
	
}
