package com.example.service;

import java.sql.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.dao.InternDao;
import com.example.dao.ManagerDao;
import com.example.dao.SalaryHistoryDao;
import com.example.dao.TraineeDao;
import com.example.model.Intern;
import com.example.model.Manager;
import com.example.model.SalaryHistory;
import com.example.model.Trainee;

@Component
public class ManageEmployee {
	
	@Autowired
	private InternDao internDao;
	
	@Autowired
	private SalaryHistoryDao salaryHistoryDao;

	
	
	@PostConstruct
	public void init(){
		Intern intern = new Intern();
		intern.firstName = "Asok";
		intern.surname = "Unknown";
		intern.hiringDate = Date.valueOf("2006-3-5");
		intern.salary = 1000;
		internDao.save(intern);
		
		for (int i = 0; i<3; i++){
			SalaryHistory salaryHistory = new SalaryHistory();
			salaryHistory.periodStartDate=Date.valueOf((2006+i)+"-3-5");
			salaryHistory.employee = intern;
			salaryHistory.salary=800+100*i;
			if (i != 2){
				salaryHistory.periodEndDate=Date.valueOf((2006+i+1)+"-3-5");
			}
			salaryHistoryDao.save(salaryHistory);
		}
		
		intern = new Intern();
		intern.firstName = "Asok";
		intern.surname = "Known";
		intern.hiringDate = Date.valueOf("2007-3-5");
		intern.salary = 1000;
		internDao.save(intern);
		
		for (int i = 0; i<3; i++){
			SalaryHistory salaryHistory = new SalaryHistory();
			salaryHistory.periodStartDate=Date.valueOf((2007+i)+"-3-5");
			salaryHistory.employee = intern;
			salaryHistory.salary=800+100*i;
			if (i != 2){
				salaryHistory.periodEndDate=Date.valueOf((2007+i+1)+"-3-5");
			}
			salaryHistoryDao.save(salaryHistory);
		}
	}
	
	
	public List<Intern> retrieveInternByNamedQuery(){
		List<Intern> i = internDao.findInternPerName("Asok");
		return i;
	}
	
	public Intern findInternById(Integer id){
		Intern i = internDao.findOne(id);
		return i;
	}
	
	public SalaryHistory findSalaryById(Integer id){
		SalaryHistory s = salaryHistoryDao.findOne(id);
		return s;
	}

	public SalaryHistory findSalaryByNamedQuery(Integer id){
		SalaryHistory s = salaryHistoryDao.findById(id);
		return s;
	}
	
}
