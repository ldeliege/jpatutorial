package com.example.controler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.Intern;
import com.example.service.ManageEmployee;

@RestController
public class SqlSniffer {
	
	@Autowired
	private ManageEmployee manageEmployee;

    @RequestMapping(path="findAsok", method = RequestMethod.GET)
    public Intern get() {
        return manageEmployee.retrieveInternByNamedQuery().get(0);
    }
	
}
